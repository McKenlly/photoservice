﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoWebService.Models
{

    public class Passport
    {
        [Key]
        [ForeignKey("Person")]
        public int Id { get; set; }
        public string Series { get; set; }
        public string Number { get; set; }
        public Person DataPerson { get; set; }
    }
}
