﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoService.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public string Descrition { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }


    }
}
