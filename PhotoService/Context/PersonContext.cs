﻿using Microsoft.EntityFrameworkCore;
using PhotoService.Context;
using PhotoWebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoWebService.Context
{
    public class PersonContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Passport> Passports { get; set; }

        public PersonContext(DbContextOptions<PersonContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new PassportConfiguration());
            modelBuilder.ApplyConfiguration(new PersonConfiguration());
        }
    }
}
