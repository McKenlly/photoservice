﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoWebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoService.Context
{
    public class PassportConfiguration : IEntityTypeConfiguration<Passport>
    {
        public void Configure(EntityTypeBuilder<Passport> builder)
        {
            builder.HasKey(b => b.Id);
            builder.Property(b => b.Number).IsRequired();
            builder.Property(b => b.Series).IsRequired();
            builder.Property(b => b.DataPerson).IsRequired();
        }
    }
}
