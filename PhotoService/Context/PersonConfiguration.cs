﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoWebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoWebService.Context
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasKey(b => b.Id);
            builder.Property(b => b.FirstName).IsRequired().HasMaxLength(50).IsUnicode(true);
            builder.Property(b => b.LastName).IsRequired().HasMaxLength(50).IsUnicode(true);
            builder.Property(b => b.Age).IsRequired();
            builder.Property(b => b.DataPasport).IsRequired();

        }
    }
}
