﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PhotoWebService.Context;
using PhotoWebService.Models;

namespace PhotoWebService.Repository
{
    public class PersonRepositoryAsync : IPersonRepositoryAsync
    {
        private readonly PersonContext _context = null;

        public PersonRepositoryAsync(PersonContext context)
        {
            _context = context;
        }


        public async Task<Int32> AddPerson(Person person)
        {
            _context.Persons.Add(person);
            return await _context.SaveChangesAsync();
        }

        

        public async Task<IEnumerable<Person>> GetAllPersons()
        {
            return await _context.Persons.ToListAsync();
        }

        public async Task<IEnumerable<Person>> GetPersonsByLastName(string name)
        {
            return await (from person in _context.Persons
                          where person.LastName == name
                          select person).ToListAsync();
        }

        public async Task DeletePersonById(Int32 id)
        {
            Person person = (Person)_context.Persons.Where(b => b.Id == id).SingleOrDefault();
            if (person != null)
            {
                _context.Persons.Remove(person);
                await _context.SaveChangesAsync();
            } 
        }

        public async Task UpdatePerson(Person person)
        {
            _context.Persons.Update(person);
            await _context.SaveChangesAsync();
        }
    }
}
