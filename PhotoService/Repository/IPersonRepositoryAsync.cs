﻿using PhotoWebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoWebService.Repository
{
    public interface IPersonRepositoryAsync
    {
        Task<IEnumerable<Person>> GetPersonsByLastName(string name);
        Task<IEnumerable<Person>> GetAllPersons();
        Task<Int32> AddPerson(Person person);
        Task UpdatePerson(Person person);
        Task DeletePersonById(Int32 id);

    }
}
