﻿using Microsoft.AspNetCore.Mvc;
using PhotoWebService.Models;
using PhotoWebService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace PhotoWebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IPersonRepositoryAsync _repository;

        public PersonController(IPersonRepositoryAsync repository)
        {
            _repository = repository;
        }

        [HttpGet("all")]
        public async Task<IEnumerable<Person>> Get()
        {
            return await _repository.GetAllPersons();
        }
               
        [HttpGet("{surname}")]
        public async Task<IEnumerable<Person>> GetByLastName(string surname)
        {
            return await _repository.GetPersonsByLastName(surname);
        }

        [HttpPost]
        public async Task Post([FromBody] Person person)
        {
            await _repository.AddPerson(person);
        }

        [HttpDelete]
        public async Task Delete(Int32 id)
        {
            await _repository.DeletePersonById(id);
        }

        [HttpPut]
        public async Task Put([FromBody] Person person)
        {
            await _repository.UpdatePerson(person);
        }
    }
}
